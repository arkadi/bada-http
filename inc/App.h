#ifndef APP_H_
#define APP_H_

#include <FApp.h>
#include <FBase.h>
#include <FSystem.h>
#include <FUi.h>
#include <FTelSimInfo.h>
#include <FLocales.h>

#include "IRPCListener.h"

using namespace Osp::App;
using namespace Osp::Base;
using namespace Osp::Base::Collection;
using namespace Osp::System;
using namespace Osp::Graphics; // for Rectangle
using namespace Osp::Ui;
using namespace Osp::Ui::Controls;
using namespace Osp::Telephony; // for SimInfo
using namespace Osp::Locales; // for LanguageCode

class App;
class ResourceManager;
class Spinner;
class RPC;
class MyData;

#include "ResourceManager.h"
#include "Spinner.h"
#include "RPC.h"
#include "MyData.h"

class App :
	public Application
{
private:
	const static String myAPIVersion;
	const static String phoneNumberPrefName;
	const static String passwordPrefName;
	const static String iccidPrefName;

	String phone;
	String password;
	String iccid;
	String lang;

	String FetchAPIVersion();

public:
	Frame *frame;
	AppResource *res;
	ResourceManager *mgr;
	RPC *rpc;

	MyData *myDataForm;

	static Application* CreateInstance(void);

	const static String myDataUri;

	App();
	~App();

	void ActivateMyDataForm(void);

	bool OnAppInitializing(AppRegistry& appRegistry);
	bool OnAppTerminating(AppRegistry& appRegistry, bool forcedTermination = false);

	virtual void RPCResultReadyN(HttpSession& httpSession, HttpTransaction& httpTransaction, NetHttpStatusCode code, String *bodyText, ByteBuffer *body, json::UnknownElement *obj, String *rpcId);
	virtual void RPCAborted(HttpSession& httpSession, HttpTransaction& httpTransaction, result r, String *rpcId);

};

#endif /* APP_H_ */
