#ifndef FORMMANAGER_H_
#define FORMMANAGER_H_

#include "App.h"
#include "IRPCListener.h"
#include <json/elements.h>

class ResourceManager :
	public Form
{
private:
	App *app;
public:
	ResourceManager(App *app);
	virtual ~ResourceManager();

	bool Initialize();

	virtual void OnUserEventReceivedN(RequestId requestId, IList* args);

	const static RequestId REMOVE = 1;
	const static RequestId RPC_RESULT = 2;
	const static RequestId RPC_ABORTED = 3;
	void remove(Form *form);
	void rpcResult( IRPCListener *listener, HttpSession& httpSession, HttpTransaction& httpTransaction, NetHttpStatusCode code, String *response, ByteBuffer *binaryResponse, json::UnknownElement *obj, String *rpcId);
	void rpcAborted(IRPCListener *listener, HttpSession& httpSession, HttpTransaction& httpTransaction, result r, String *rpcId);
};

#endif /* FORMMANAGER_H_ */
