#ifndef SPINNER_H_
#define SPINNER_H_

#include <FBase.h>
#include <FUi.h>
#include <FMedia.h>
#include <FGraphics.h>

using namespace Osp::Base;
using namespace Osp::Base::Collection;
using namespace Osp::Ui;
using namespace Osp::Ui::Controls;
using namespace Osp::Media; // Image
using namespace Osp::Graphics; // Bitmap

enum SpinnerStyle {
	BLUE_SMALL, BLUE_LARGE, ORANGE_SMALL, ORANGE_LARGE, WHITE_SMALL, WHITE_LARGE
};

class Spinner {
private:
	Image *img;
	Animation *animation;
	bool active;
	Bitmap*	GetBitmapN(const String& file);
public:
	Spinner(Osp::Ui::Container *cont, Rectangle rect, SpinnerStyle style, IAnimationEventListener *listener = 0);
	virtual ~Spinner();
	void Start(void);
	void Show(void);
	void Stop(void);
	void Hide(bool redraw = true);

	const static int Small = 56;
	const static int Large = 120;
};

#endif /* SPINNER_H_ */
