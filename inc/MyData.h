#ifndef MYDATA_H_
#define MYDATA_H_

#include "App.h"

class MyData :
	public Form,
	public IActionEventListener,
	public IRPCListener
{
private:
	App *app;
	String *data;
	Popup *popup;
	Spinner *spinner;

	static const int ID_BUTTON_REFRESH = 1;
	static const int ID_BUTTON_CLOSE = 2;

public:
	MyData(App *app);
	virtual ~MyData(void);

	bool Initialize(void);

	// Form
	virtual result OnInitializing(void);
	virtual result OnTerminating(void);
	virtual result Show(void);
	// IActionEventListener
	virtual void OnActionPerformed(const Control& source, int actionId);
	// IRPCListener
	virtual void RPCResultReadyN(HttpSession& httpSession, HttpTransaction& httpTransaction, NetHttpStatusCode code, String *bodyText, ByteBuffer *body, json::UnknownElement *obj, String *rpcId);
	virtual void RPCAborted(HttpSession& httpSession, HttpTransaction& httpTransaction, result r, String *rpcId);
};

#endif /* MYDATA_H_ */
