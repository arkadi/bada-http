#ifndef IRPCLISTENER_H_
#define IRPCLISTENER_H_

#include <json/elements.h>
#include <FBase.h>
#include <FNet.h>

using namespace Osp::Base;
using namespace Osp::Net::Http;

class IRPCListener
{
public:
	~IRPCListener() {}
	virtual void RPCResultReadyN(HttpSession& httpSession, HttpTransaction& httpTransaction, NetHttpStatusCode code, String *bodyText, ByteBuffer *body, json::UnknownElement *obj, String *rpcId) = 0;
	virtual void RPCAborted(HttpSession& httpSession, HttpTransaction& httpTransaction, result r, String *rpcId) = 0;
};

#endif /* IRPCLISTENER_H_ */
