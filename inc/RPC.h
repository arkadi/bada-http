#ifndef RPC_H_
#define RPC_H_

#include <FBase.h>
#include <FNet.h>
#include <FText.h>

#include "IRPCListener.h"
#include "ResourceManager.h"

using namespace Osp::Base;
using namespace Osp::Base::Collection;
using namespace Osp::Net::Http;
using namespace Osp::Text;

class RPC :
	public IHttpTransactionEventListener
{
private:
	ResourceManager *mgr;
	String baseURL;
	HttpSession *session;
	struct ListenerPlusRpcId {
		IRPCListener *listener;
		String *rpcId;
	};
	HashMapT<HttpTransaction*, ListenerPlusRpcId*> map;

public:
	RPC(ResourceManager *mgr, const String& baseURL);
	virtual ~RPC();

	virtual HttpTransaction* call(IRPCListener *listener, String uri, ByteBuffer *data = 0, String *rpcId = 0);
	virtual HttpTransaction* call(IRPCListener *listener, String uri, String &str, String *rpcId = 0);

	virtual void OnTransactionReadyToRead(HttpSession& httpSession, HttpTransaction& httpTransaction, int availableBodyLen);
	virtual void OnTransactionAborted(HttpSession& httpSession, HttpTransaction& httpTransaction, result r);
	virtual void OnTransactionReadyToWrite(HttpSession& httpSession, HttpTransaction& httpTransaction, int recommendedChunkSize);
	virtual void OnTransactionHeaderCompleted(HttpSession& httpSession, HttpTransaction& httpTransaction, int headerLen, bool bAuthRequired);
	virtual void OnTransactionCompleted(HttpSession& httpSession, HttpTransaction& httpTransaction);
	virtual void OnTransactionCertVerificationRequiredN(HttpSession& httpSession, HttpTransaction& httpTransaction, String* pCert);
};

#endif /* RPC_H_ */
