#include <stdio.h>
#include "Spinner.h"

Spinner::Spinner(Osp::Ui::Container *cont, Rectangle rect, SpinnerStyle style, IAnimationEventListener *listener) :
	img(0),
	animation(0),
	active(false)
{
	String dir(64);
	String ext(8);

	switch(style) {
		case BLUE_SMALL: case ORANGE_SMALL: case WHITE_SMALL: ext = ".png";     dir = "56x56/";   break;
		case BLUE_LARGE: case ORANGE_LARGE: case WHITE_LARGE: ext = "_big.png"; dir = "120x120/"; break;
	}
	// 56x56/ ... .png
	switch(style) {
		case BLUE_SMALL:   case BLUE_LARGE:   dir.Insert("blue/", 0);   break;
		case ORANGE_SMALL: case ORANGE_LARGE: dir.Insert("orange/", 0); break;
		case WHITE_SMALL:  case WHITE_LARGE:  dir.Insert("white/", 0);  break;
	}
	// blue/56x56/ ... .png
	dir.Insert("/Res/spinner/", 0);
	// /Res/spinner/blue/56x56/ ... .png
	dir.Append("progressing");
	// /Res/spinner/blue/56x56/progressing ... .png

	img = new Image();
	img->Construct();

	const int n = 8;
	long duration = 500/n;
	ArrayList list;
	list.Construct(n);
	for (int i = 0; i < n; ++i) {
		String path(64);
		path = dir;

		char index[4];
		sprintf(index, "%02d", i);
		path.Append(String(index));
		// /Res/spinner/blue/56x56/progressing00

		path.Append(ext);
		// /Res/spinner/blue/56x56/progressing00.png

		Bitmap *bitmap = GetBitmapN(path);
		if (bitmap == 0) {
			for (int j = 0; j < list.GetCount(); ++j)
				delete list.GetAt(j);
			delete img;
			img = 0;
			return;
		}
		AnimationFrame *frame = new AnimationFrame(*bitmap, duration);
		list.Add(*frame);
		delete bitmap;
	}

	animation = new Animation();
	animation->Construct(rect, list);
	animation->SetRepeatCount(10000);
	if (listener != 0)
		animation->AddAnimationEventListener(*listener);
	cont->AddControl(*animation);

	delete img;
	img = 0;
}

Bitmap*
Spinner::GetBitmapN(const String& file)
{
	Bitmap* bitmap = 0;
	if (file.EndsWith("jpg") || file.EndsWith("bmp") || file.EndsWith("gif")) {
		bitmap = img->DecodeN(file, BITMAP_PIXEL_FORMAT_RGB565);
	} else if (file.EndsWith("png")) {
		bitmap = img->DecodeN(file, BITMAP_PIXEL_FORMAT_ARGB8888);
	} else {
		AppLog("Cannot handle image %ls", file.GetPointer());
		SetLastResult(E_INVALID_ARG);
	}
	return bitmap;
}

Spinner::~Spinner()
{
	if (img != 0) delete img;
	if (animation != 0) delete animation;
}

void
Spinner::Start(void)
{
	if (animation == 0 || active) return;
	active = true;
	animation->Play();
}

void
Spinner::Show(void)
{
	Start();
	if (animation == 0) return;
	animation->SetShowState(true);
	animation->Draw();
	animation->Show();
}

void
Spinner::Stop(void)
{
	if (animation == 0 || !active) return;
	active = false;
	animation->Stop();
}

void
Spinner::Hide(bool redraw)
{
	Stop();
	if (animation == 0) return;
	animation->SetShowState(false);
	if (redraw) {
		Osp::Ui::Container *cont = animation->GetParent();
		if (cont != 0) {
			cont->Draw();
			cont->Show();
		}
	}
}
