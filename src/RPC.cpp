#include <sstream>
#include <json/reader.h>
#include <json/writer.h>
#include "RPC.h"
#include <zlib.h>

RPC::RPC(ResourceManager *mgr, const String &baseURL) :
	mgr(mgr),
	baseURL(baseURL),
	session(0)
{
	map.Construct();
}

RPC::~RPC() {
	if (session != 0) delete session;
}

// TODO: manual cookie management - due to destruction of HttpSession after E_INVALID_SESSION (lost wifi/3g link)
HttpTransaction*
RPC::call(IRPCListener *listener, String uri, ByteBuffer *data, String *rpcId)
{
    result r;
    if (session == 0) {
        session = new HttpSession();
        r = session->Construct(NET_HTTP_SESSION_MODE_NORMAL, 0, baseURL, 0 /*, NET_HTTP_COOKIE_FLAG_ALWAYS_AUTOMATIC */);
        if (IsFailed(r)) {
            AppLog("HttpSession::Construct() failed: %s", GetErrorMessage(r));
            delete session;
            session = 0;
            return 0;
        }
    }
    HttpTransaction *transaction = session->OpenTransactionN();
    if (transaction == 0) {
        r = GetLastResult();
        AppLogException("HttpSession::OpenTransactionN() failed: %s", GetErrorMessage(r));
        return 0;
    }
    HttpRequest *request = transaction->GetRequest();
    if(request == 0) {
        r = GetLastResult();
        AppLogException("HttpTransaction::GetRequest() failed: %s", GetErrorMessage(r));
        delete transaction;
        return 0;
    }
    r = request->SetUri(baseURL + uri);
    if(IsFailed(r)) {
        AppLogException("HttpRequest::GetRequest() failed: %s", GetErrorMessage(r));
        delete transaction;
        return 0;
    }
    NetHttpMethod  method;
    if (data == 0)
        method = NET_HTTP_METHOD_GET;
    else
        method = NET_HTTP_METHOD_POST;
    r = request->SetMethod(method);
    if(IsFailed(r)) {
        AppLogException("HttpRequest::SetMethod() failed: %s", GetErrorMessage(r));
        delete transaction;
        return 0;
    }
    HttpHeader *header = request->GetHeader();
    r = header->AddField("Accept-Encoding", "deflate");
    if(IsFailed(r))
        AppLogException("HttpRequest::AddField() failed: %s", GetErrorMessage(r));

    if (data != 0) {
        r = request->WriteBody(*data);
        if(IsFailed(r)) {
            AppLogException("HttpRequest::WriteBody() failed: %s", GetErrorMessage(r));
            delete transaction;
            return 0;
        }
    }
    r = transaction->AddHttpTransactionListener(*this);
    if (IsFailed(r)) {
        AppLogException("HttpTransaction::AddHttpTransactionListener() failed: %s", GetErrorMessage(r));
        delete transaction;
        return 0;
    }
    ListenerPlusRpcId *l = new ListenerPlusRpcId();
    l->listener = listener;
    l->rpcId = rpcId;
    map.Add(transaction, l);
    r = transaction->Submit();
    if(IsFailed(r)) {
        AppLogException("HttpTransaction::Submit() failed: %s", GetErrorMessage(r));
        map.Remove(transaction);
        delete l;
        delete transaction;
        if (r == E_INVALID_SESSION) {
            delete session;
            session = 0;
        }
        return 0;
    }
    return transaction;
}

HttpTransaction*
RPC::call(IRPCListener *listener, String uri, String &str, String *rpcId)
{
	Utf8Encoding utf8;
	ByteBuffer *data = utf8.GetBytesN(str);
	HttpTransaction *transaction = call(listener, uri, data, rpcId);
	delete data;
	return transaction;
}

void RPC::OnTransactionReadyToRead(HttpSession& httpSession, HttpTransaction& httpTransaction, int availableBodyLen)
{
}

void RPC::OnTransactionReadyToWrite(HttpSession& httpSession, HttpTransaction& httpTransaction, int recommendedChunkSize)
{
}

void RPC::OnTransactionHeaderCompleted(HttpSession& httpSession, HttpTransaction& httpTransaction, int headerLen, bool bAuthRequired)
{
}

void RPC::OnTransactionAborted(HttpSession& httpSession, HttpTransaction& httpTransaction, result r)
{
	AppLog("-------------------------------- aborted: %s", GetErrorMessage(r));
	ListenerPlusRpcId *l;
	map.GetValue(&httpTransaction, l);
	map.Remove(&httpTransaction);
	mgr->rpcAborted(l->listener, httpSession, httpTransaction, r, l->rpcId);
	session = 0;
	delete l;
}

void RPC::OnTransactionCompleted(HttpSession& httpSession, HttpTransaction& httpTransaction)
{
	ListenerPlusRpcId *l;
	map.GetValue(&httpTransaction, l);
	map.Remove(&httpTransaction);

	HttpResponse *response = httpTransaction.GetResponse();
	NetHttpStatusCode code = response->GetStatusCode();
	HttpHeader *header = response->GetHeader();

	IEnumerator *contentEncodingE = header->GetFieldValuesN("Content-Encoding");
	bool deflated = false;
	if (contentEncodingE != 0) {
		contentEncodingE->MoveNext();
		String *contentEncoding = dynamic_cast<String *>(contentEncodingE->GetCurrent());
		if (*contentEncoding == "deflate") {
			deflated = true;
		} else {
			AppLog("not implemented Content-Encoding: %ls", contentEncoding->GetPointer());
		}
		delete contentEncodingE;
	}
	// TODO must also check Transfer-Encoding

	IEnumerator *contentTypeE = header->GetFieldValuesN("Content-Type");
	bool text = false;
	bool json = false;
	if (contentTypeE != 0) {
		// TODO must also check charset= part
		contentTypeE->MoveNext();
		String *contentType = dynamic_cast<String *>(contentTypeE->GetCurrent());
		int dummy;
		if (contentType->StartsWith("text/", 0) ||
			(contentType->StartsWith("application/", 0) && contentType->IndexOf("xml", 12, dummy) == E_SUCCESS))
			text = true;
		else if (*contentType == String("application/json")) {
			text = true;
			json = true;
		}
		delete contentTypeE;
	}

	AppLogDebug("-------------------------------- completed: %d %ls\n", code, response->GetStatusText().GetPointer());
	ByteBuffer *body = response->ReadBodyN();
	String *bodyText = 0;
	std::string *jsonText = 0;
	json::UnknownElement *obj = 0;
	if (text) {
		int len = body->GetLimit();
		byte *content = new byte[len+1]; // +1 for '\0'
		body->GetArray(content, 0, len);
		delete body;
		body = 0;
		if (!deflated) {
			content[len] = '\0';
			const char *str = reinterpret_cast<const char *>(content);
			if (!json)
				bodyText = new String(str); // assume utf-8
			else
				jsonText = new std::string(str);
		} else {
			unsigned int buflen = 4096;
			char buf[buflen];

			z_stream strm;
		    strm.zalloc = Z_NULL;
		    strm.zfree = Z_NULL;
		    strm.opaque = Z_NULL;
		    strm.avail_in = 0;
		    strm.next_in = Z_NULL;
		    int ret = inflateInit(&strm);
		    AppLogDebug("ZLIB inflateInit(): %d", ret);
		    if (ret != Z_OK) {
		    	delete[] content;
		    	mgr->rpcAborted(l->listener, httpSession, httpTransaction, E_OUT_OF_MEMORY, l->rpcId);
		    	if (&httpSession == session) session = 0;
		    	delete l;
		    	return;
		    }
		    if (!json)
		    	bodyText = new String(len*2);
		    else {
				jsonText = new std::string;
				jsonText->reserve(len*2);
		    }
		    strm.next_in = content;
		    strm.avail_in = len;
		    do {
				strm.next_out = reinterpret_cast<unsigned char *>(buf);
				strm.avail_out = buflen-1; // -1 for '\0'
				ret = inflate(&strm, Z_NO_FLUSH);
			    AppLogDebug("ZLIB inflate(): %d", ret);
				if (ret != Z_OK && ret != Z_STREAM_END) {
				    inflateEnd(&strm);
				    if (!json)
				    	delete bodyText;
				    else
				    	delete jsonText;
					delete[] content;
					mgr->rpcAborted(l->listener, httpSession, httpTransaction, E_IO, l->rpcId);
			    	if (&httpSession == session) session = 0;
					delete l;
					return;
				}
				if (strm.avail_out < buflen-1) {
					buf[buflen - 1 - strm.avail_out] = '\0';
					if (!json)
						bodyText->Append(String(buf)); // assume utf-8
					else
						jsonText->append(buf);
				}
		    } while (ret != Z_STREAM_END && strm.avail_in > 0); // why inflate() sometimes returns Z_OK but avail_in is 0?
		    ret = inflateEnd(&strm);
		    AppLogDebug("ZLIB inflateEnd(): %d", ret);
		}
		if (json) {
			obj = new json::UnknownElement;
			std::istringstream stdi(*jsonText);
			try {
				json::Reader::Read(*obj, stdi);
				delete jsonText;
			} catch (json::Exception& e) {
				AppLog("exception parsing JSON: %s", e.what());
				delete obj;
				delete jsonText;
				mgr->rpcAborted(l->listener, httpSession, httpTransaction, E_PARSING_FAILED, l->rpcId);
		    	if (&httpSession == session) session = 0;
				delete l;
				return;
			}
		}
	} // else binary
	mgr->rpcResult(l->listener, httpSession, httpTransaction, code, bodyText, body, obj, l->rpcId);
	delete l;
}

void RPC::OnTransactionCertVerificationRequiredN(HttpSession& httpSession, HttpTransaction& httpTransaction, String* pCert)
{
	AppLog("%ls", pCert->GetPointer());
}
