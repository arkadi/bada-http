#include "MyData.h"
#include "zlib.h"

using namespace Osp::Base;
using namespace Osp::Ui;
using namespace Osp::Ui::Controls;

MyData::MyData(App *app) :
	app(app),
	data(0)
{
}

MyData::~MyData(void)
{
	if (data != 0)
		delete data;
}

bool
MyData::Initialize()
{
	Construct("IDF_MYDATA");
	return true;
}

result
MyData::OnInitializing(void)
{
	result r = E_SUCCESS;
	popup = new Popup();
	popup->Construct(true, Dimension(388, 388));
	popup->SetTitleText("Loading...");
	Rectangle b = popup->GetClientAreaBounds();
	spinner = new Spinner(popup, Rectangle((b.width-Spinner::Large)/2, (b.height-Spinner::Large)/2, Spinner::Large, Spinner::Large), ORANGE_LARGE);
	SetSoftkeyActionId(SOFTKEY_0, ID_BUTTON_REFRESH);
	AddSoftkeyActionListener(SOFTKEY_0, *this);
	SetSoftkeyActionId(SOFTKEY_1, ID_BUTTON_CLOSE);
	AddSoftkeyActionListener(SOFTKEY_1, *this);

	return r;
}

result
MyData::OnTerminating(void)
{
	result r = E_SUCCESS;
	return r;
}

result
MyData::Show(void)
{
	if (data == 0) {
		if (app->rpc->call(this, App::myDataUri) != 0) {
			popup->SetShowState(true);
			popup->Draw();
			popup->Show();
			spinner->Start();
		} else {
			//app->Activate...Form();
		}
	}
	result r = Form::Show();
	return r;
}

void
MyData::OnActionPerformed(const Control& source, int actionId)
{
	AppLogDebug("action: %d", actionId);
	switch (actionId) {
	case ID_BUTTON_REFRESH:
		if (app->rpc->call(this, App::myDataUri) != 0) {
			popup->SetShowState(true);
			popup->Draw();
			popup->Show();
			spinner->Start();
		}
		break;

	case ID_BUTTON_CLOSE:
		//app->Activate...Form();
		break;
	}
}

void
MyData::RPCResultReadyN(HttpSession& httpSession, HttpTransaction& httpTransaction, NetHttpStatusCode code, String *bodyText, ByteBuffer *body, json::UnknownElement *obj, String *rpcId)
{
	Label *label = dynamic_cast<Label *>(GetControl(L"IDC_MYDATA"));
	if (bodyText != 0) {
		if (data != 0)
			delete data;
		data = bodyText;
		label->SetText(*data);
	} else if (obj != 0) {
		std::string *text = 0;
		try {
			const json::Object& j = *obj;
			const json::String& name = j["name"];
			const json::String& address = j["address"];
			text = new std::string;
			*text += "name: ";
			*text += name.Value();
			*text += "\n";
			*text += "address: ";
			*text += address.Value();
		} catch (json::Exception& e) {}
		delete obj;
		if (text != 0) {
			if (data != 0)
				delete data;
			data = new String(text->c_str());
			SimInfo i;
			i.Construct();
			*data += "\nICC-ID:\n" + i.GetIccId();
			AppLog("ICC-ID: %ls", i.GetIccId().GetPointer());
			label->SetText(*data);
		}
	} else
		label->SetText("(no data)");
	spinner->Stop();
	popup->SetShowState(false);
	Draw();
	Show();
}

void
MyData::RPCAborted(HttpSession& httpSession, HttpTransaction& httpTransaction, result r, String *rpcId)
{
	spinner->Stop();
	popup->SetShowState(false);
	if (data == 0) {
		app->ActivateMenuForm();
	} else {
		Draw();
		Show();
	}
}
