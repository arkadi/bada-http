#include "App.h"

const String App::myAPIVersion("1");
const String App::baseUrl("http://domain.com");
const String App::myDataUri("/app/mydata.json");

App::App() :
	myDataForm(0)
{
}

App::~App()
{
}

Application*
App::CreateInstance(void)
{
	return new App();
}

bool
App::OnAppInitializing(AppRegistry& appRegistry)
{
	frame = GetAppFrame()->GetFrame();
	res = GetAppResource();
	// resource manager - fake form for event passing and object clean-up
	mgr = new ResourceManager(this);
	mgr->Initialize();
	frame->AddControl(*mgr);
	rpc = new RPC(mgr, baseUrl);

	ActivateMyDataForm();

	return true;
}

String App::FetchAPIVersion()
{
	String v("1");
	return v;
}

void App::ActivateMyDataForm()
{
	if (myDataForm == 0) {
		AppLogDebug("initializing MyDataForm");
		myDataForm = new MyData(this);
		myDataForm->Initialize();
		frame->AddControl(*myDataForm);
	}
	frame->SetCurrentForm(*myDataForm);
	myDataForm->Draw();
	myDataForm->Show();
}

bool
App::OnAppTerminating(AppRegistry& appRegistry, bool forcedTermination)
{
	// TODO:
	// Deallocate resources allocated by this application for termination.
	// The application's permanent data and context can be saved via appRegistry.
	return true;
}
