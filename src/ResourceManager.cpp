#include "ResourceManager.h"

ResourceManager::ResourceManager(App *app) :
	app(app)
{
}

ResourceManager::~ResourceManager()
{
}

bool
ResourceManager::Initialize()
{
	Construct(FORM_STYLE_NORMAL);
	SetName("FormManager");
	return true;
}

void
ResourceManager::OnUserEventReceivedN(RequestId requestId, IList* args)
{
	switch (requestId) {
	case REMOVE:
		app->frame->RemoveControl(*(dynamic_cast<Form *>(args->GetAt(0))));
		break;
	case RPC_ABORTED:
		delete (dynamic_cast<HttpTransaction *>(args->GetAt(0)));
		delete (dynamic_cast<HttpSession *>(args->GetAt(1)));
		break;
	case RPC_RESULT:
		delete (dynamic_cast<HttpTransaction *>(args->GetAt(0)));
		break;
	}
	delete args;
}

void
ResourceManager::remove(Form *form)
{
	ArrayList *args = new ArrayList;
	args->Construct(1);
	args->Add(*form);
	SendUserEvent(REMOVE, args);
}

void
ResourceManager::rpcResult(IRPCListener *listener, HttpSession& httpSession, HttpTransaction& httpTransaction, NetHttpStatusCode code, String *bodyText, ByteBuffer *body, json::UnknownElement *obj, String *rpcId)
{
	listener->RPCResultReadyN(httpSession, httpTransaction, code, bodyText, body, obj, rpcId);
	ArrayList *args = new ArrayList;
	args->Construct(1);
	args->Add(httpTransaction);
	SendUserEvent(RPC_RESULT, args);
}

void
ResourceManager::rpcAborted(IRPCListener *listener, HttpSession& httpSession, HttpTransaction& httpTransaction, result r, String *rpcId)
{
	listener->RPCAborted(httpSession, httpTransaction, r, rpcId);
	httpSession.CloseTransaction(httpTransaction); // XXX is it needed?
	ArrayList *args = new ArrayList;
	args->Construct(2);
	args->Add(httpTransaction);
	args->Add(httpSession);
	SendUserEvent(RPC_ABORTED, args);
}
